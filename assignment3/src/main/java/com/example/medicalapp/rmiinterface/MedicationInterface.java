package com.example.medicalapp.rmiinterface;
import java.rmi.*;

public interface MedicationInterface extends Remote {
    public String getMedName() throws RemoteException;

    public void setMedName(String medName);

    public int getDosage();

    public void setDosage(int dosage);

    public String getTimeToTake();

    public void setTimeToTake(String timeToTake);

    public String toString();
}
