package com.example.medicalapp.rmiclient.gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.*;

public class ClientView extends JFrame{
        private JPanel content = new JPanel();

        private JPanel medPlans = new JPanel();

       private JTextArea medPlansTxt = new JTextArea("");
       
       private JLabel yourClock = new JLabel();
       
       public void clear() {
    	   medPlansTxt.setText("");
       }

        public void addMedPlanToView(String mp){
           medPlansTxt.append(mp + "\n");
        }


        public ClientView() {
            this.setContentPane(content);
            content.setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));
            content.add(medPlans);
            medPlans.add(medPlansTxt);
            medPlans.add(yourClock);
            this.setTitle("Patient");
            this.setSize(800, 300);
            
            ActionListener updateClockAction = new ActionListener() {
            	  public void actionPerformed(ActionEvent e) {
            	      yourClock.setText(new Date().toString()); 
            	    }
            };
            
            Timer t = new Timer(1000, updateClockAction);
            t.start();
            
            setVisible();
            
        }
        
       
        public void setVisible(){
            this.setVisible(true);
        }
    }

