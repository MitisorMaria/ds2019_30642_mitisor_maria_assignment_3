package com.example.medicalapp;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.medicalapp.model.MedicationPlan;
import com.example.medicalapp.repository.MedicationPlanMapper;
import com.example.medicalapp.rmiinterface.MedicationInterface;
import com.example.medicalapp.rmiinterface.RMIInterface;


@SpringBootApplication
public class MedicalAppApplication extends UnicastRemoteObject implements RMIInterface{
	
	private MedicationPlanMapper medPlanMapper = new MedicationPlanMapper();
	
	private static final long serialVersionUID = 1L;

	private  List<MedicationPlan> medicationPlan  = new ArrayList<MedicationPlan>();

	
	
	protected MedicalAppApplication() throws RemoteException {
		super();
	}


	public static void main(String[] args) {
		SpringApplication.run(MedicalAppApplication.class, args);
		try {
			MedicalAppApplication app = new MedicalAppApplication();
			Naming.rebind("//localhost/MyServer", app);            
            System.err.println("Server ready");
            app.medPlanMapper.getMedPlans().iterator().forEachRemaining(app.medicationPlan::add);
            while(true);
            
        } catch (Exception e) {
        	System.err.println("Server exception: " + e.toString());
          e.printStackTrace();
        }
	}


	@Override
	public ArrayList<MedicationInterface> getMedPlan(int patId) throws RemoteException {
		ArrayList<MedicationInterface> medplans = new ArrayList<>();
		for (MedicationPlan mp : medicationPlan) {
			if (mp.getPatientId() ==  patId) {
				medplans.add(mp);
			}
		}
		return medplans;
		
	}

}
