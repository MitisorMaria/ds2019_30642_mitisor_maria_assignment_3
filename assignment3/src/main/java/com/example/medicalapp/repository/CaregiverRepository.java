package com.example.medicalapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.medicalapp.model.Caregiver;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, String>{

}
