package com.example.medicalapp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "patients")
public class Patient implements Serializable{
	@Id
	private String id;
	@Column(name = "name")
	private String name;
	@Column(name = "birth_date")
	private String birthDate;
	@Column(name = "gender")
	private String gender;
	@Column(name = "address")
	private String address;
	@Column(name = "medical_record")
	private String medicalRecord;
	@Column(name = "caregiver_id")
	private String caregiverId;
	
	public Patient() {
		
	}
	
	
	public Patient(String name, String id, String birthDate, String gender, String address, String medicalRecord) {
		super();
		this.id = id;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.medicalRecord = medicalRecord;
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMedicalRecord() {
		return medicalRecord;
	}
	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}


	public String getCaregiverId() {
		return caregiverId;
	}


	public void setCaregiverId(String caregiverId) {
		this.caregiverId = caregiverId;
	}
	
	
	

}
