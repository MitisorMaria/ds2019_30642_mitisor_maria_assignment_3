package com.example.medicalapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.medicalapp.model.Patient;
import com.example.medicalapp.repository.PatientRepository;

	@Service
	@Transactional
	public class PatientServiceImpl implements PatientService {
		
		@Autowired
		private PatientRepository patientRepository;

		public List<Patient> findAll() {
			List<Patient> list = new ArrayList<>();
			patientRepository.findAll().iterator().forEachRemaining(list::add);
			return list;
		}

		@Override
		public void delete(Patient p) {
			patientRepository.delete(p);
		}


		@Override
		public Patient getPatientById(String id) {
			Optional<Patient> optionalPatient = patientRepository.findById(id);
			return optionalPatient.isPresent() ? optionalPatient.get() : null;
		}

	    @Override
	    public Patient update(Patient patientDetails) throws Exception {
		    Patient updatedPatient = patientRepository.save(patientDetails);
		    return updatedPatient;
		    
	    }

	    @Override
	    public Patient save(Patient patient) {
	        return patientRepository.save(patient);
	    }
	}
